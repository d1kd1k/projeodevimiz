﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using ProjeOdev.Models.entity;
using ProjeOdev.Models.context;
using System.Web.Security;
namespace ProjeOdev.Controllers
{
    public class AdminController : Controller
    {
        private projeContext db = new projeContext();
        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Kayit()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Kayit(Uye model)
        {
            if (ModelState.IsValid)
            {
                model.YetkiID = 2;
                db.Uyeler.Add(model);
                db.SaveChanges();
                FormsAuthentication.SetAuthCookie(model.Email, false);
                return RedirectToAction("Giris", "Admin",new {email= model.Email,sifre=model.Sifre });
            }
            else
            {
                return View();
            }
        }
        public ActionResult Giris(string email,string sifre)
        {
            var veri = db.Uyeler.ToList();
            foreach(var x in veri)
            {
                if(x.Email == email && x.Sifre == sifre)
                {
                    Session["yetki"] = x.ID;
                    return RedirectToAction("Index", "Home");
                }
            }
            return View();
            
        }
        public ActionResult Cıkıs()
        {
            Session["yetki"] = null;
            Session.Abandon();
            return RedirectToAction("Index", "Home");

        }
        public ActionResult Profil()
        {
            int s = (int)Session["yetki"];
            var data = db.Uyeler.Find(s);
            return View(data);

        }
        [HttpGet]
        public ActionResult ProfilDüzenle()
        {
            int s = (int)Session["yetki"];
            var data = db.Uyeler.Find(s);
            return View(data);

        }
        [HttpPost]
        public ActionResult ProfilDüzenle(Uye model)
        {
            if (ModelState.IsValid)
            {
                Uye yeni = db.Uyeler.Find(model.ID);

                yeni.UyeAdi = model.UyeAdi;
                yeni.UyeSoyadi = model.UyeSoyadi;
                yeni.Email = model.Email;
                yeni.Sifre =model.Sifre;
                yeni.YetkiID = 2;
                db.SaveChanges();
                return RedirectToAction("Index");

            }
            return View();

        }

    }
}