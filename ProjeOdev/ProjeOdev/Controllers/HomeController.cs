﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProjeOdev.Models.context;
using ProjeOdev.Models.entity;

namespace ProjeOdev.Controllers
{
    public class HomeController : Controller
    {
        private projeContext db = new projeContext();
        public ActionResult Index()
        {
           
            return View();
        }
        public ActionResult Vizyon()
        {
            return View();
        }
        

        // GET: Proje
        public ActionResult Projeler()
        {
            return View(db.Projeler.ToList());
        }
        [HttpGet]
        public ActionResult ProjeDetay(int id)
        {
            var proje = db.Projeler.Where(m => m.ID == id).SingleOrDefault();
            if (proje == null)
            {
                return HttpNotFound();

            }
            return View(proje);
        }
        [HttpPost]
        public ActionResult ProjeDetay(string yorum,string id)
        {
            if (ModelState.IsValid)
            {
                var yeni = new Yorum();
                yeni.YorumIcerik = yorum;
                yeni.ProjeID = Int32.Parse(id);
                int x = (int)Session["yetki"];
                yeni.UyeID = x;
                db.Yorumlar.Add(yeni);
                db.SaveChanges();
                return RedirectToAction("ProjeDetay",yeni.ProjeID);
            }
            return View();
        }
    }
}