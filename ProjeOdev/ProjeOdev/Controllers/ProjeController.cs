﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ProjeOdev.Models.context;
using ProjeOdev.Models.entity;

namespace ProjeOdev.Controllers
{
    public class ProjeController : Controller
    {
        private projeContext db = new projeContext();

        // GET: Proje
        public ActionResult Index()
        {
            int c=0;
            if (Session["yetki"]!=null)
            {
                c = (int)Session["yetki"];
                var data = db.Uyeler.Find(c);
                if (data.YetkiID == 1)
                {
                    ViewData["admin"] = "admin";
                    return View(db.Projeler.OrderByDescending(x => x.ID).ToList());
                }
                    
            }
            return View(db.Projeler.Where(x=>x.UyeID==c).ToList());
        }

        // GET: Proje/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Proje proje = db.Projeler.Find(id);
            ViewBag.kisi = db.Uyeler.Find(proje.UyeID).Email;
            if (proje == null)
            {
                return HttpNotFound();
            }
            return View(proje);
        }

        // GET: Proje/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Proje/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,ProjeBaslik,ProjeIcerik,Resim,Tarih,UyeID")] Proje proje, HttpPostedFileBase foto,string icerik)
        {
            if (ModelState.IsValid)
            {
                Proje yeni = new Proje();
                if (foto != null)
                {
                    foto.SaveAs(Server.MapPath("~/img/") + foto.FileName);
                    yeni.Resim = "/img/" + foto.FileName;
                }
                else
                    yeni.Resim = "";             
                yeni.ProjeBaslik = proje.ProjeBaslik;
                yeni.ProjeIcerik = icerik;
                yeni.Tarih = DateTime.Now;
                int c = (int)Session["yetki"];
                var data = db.Uyeler.Find(c);
                yeni.UyeID =data.ID ;
                db.Projeler.Add(yeni);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(proje);
        }

        // GET: Proje/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Proje proje = db.Projeler.Find(id);
            if (proje == null)
            {
                return HttpNotFound();
            }
            return View(proje);
        }

        // POST: Proje/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,ProjeBaslik,ProjeIcerik,Resim,Tarih,UyeID")] Proje proje, HttpPostedFileBase foto, string icerik)
        {
            if (ModelState.IsValid)
            {
                Proje yeni = db.Projeler.Find(proje.ID);
                if (foto != null)
                {
                    foto.SaveAs(Server.MapPath("~/img/") + foto.FileName);
                    yeni.Resim = "/img/" + foto.FileName;
                }
                else
                    yeni.Resim = "";
                
                yeni.ProjeBaslik = proje.ProjeBaslik;
                yeni.ProjeIcerik = icerik;
               
                yeni.Tarih = DateTime.Now;
                int c = (int)Session["yetki"];
                var data = db.Uyeler.Find(c);
                yeni.UyeID = data.ID;
                db.SaveChanges();
                return RedirectToAction("Index");

            }
            return View(proje);
        }

        // GET: Proje/Delete/5
        public ActionResult Delete(int? id)
        {
            int x = (int)Session["yetki"];
            var data = db.Uyeler.Find(x);
            if (data.YetkiID == 2)
                return RedirectToAction("Index","Admin");
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Proje proje = db.Projeler.Find(id);
            if (proje == null)
            {
                return HttpNotFound();
            }
            return View(proje);
        }

        // POST: Proje/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Proje proje = db.Projeler.Find(id);
            db.Projeler.Remove(proje);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
