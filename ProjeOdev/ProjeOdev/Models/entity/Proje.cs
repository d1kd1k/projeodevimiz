﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace ProjeOdev.Models.entity
{
    public class Proje
    {
        public int ID { get; set; }
        [Required(ErrorMessage ="Başlık gir")]
        [Display(Name ="Proje Başlığı")]
        public string ProjeBaslik { get; set; }
        [Display(Name = "Proje İçeriği")]
        public string ProjeIcerik { get; set; }
        public string Resim { get; set; }
        public DateTime Tarih { get; set; }
        [Display(Name = "Uye")]
        public int UyeID { get; set; }
        public virtual ICollection<Yorum> Yorumlar { get; set; }

    }
}