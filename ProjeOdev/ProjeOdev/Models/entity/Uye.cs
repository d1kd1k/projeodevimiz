﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace ProjeOdev.Models.entity
{
    public class Uye
    {
        public int ID { get; set; }
        [Required(ErrorMessage ="Adınızı Giriniz")]
        public string UyeAdi { get; set; }
        [Required(ErrorMessage = "Soyadınız Giriniz")]
        public string UyeSoyadi { get; set; }
        [Required(ErrorMessage = "Email Giriniz")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Sifre Giriniz")]
        public string Sifre { get; set; }
        public int YetkiID { get; set; }
        public virtual Yetki Yetki { get; set; }
        public virtual ICollection<Yorum> Yorumlar { get; set; }
    }
}